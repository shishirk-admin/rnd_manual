
.PHONY: help
help: ## Display detailed help
	@echo "To render the content use: make render, which outputs html in _output folder"
	@echo "To render PDF book: make pdf. the resulting pdf can be found in _output"
	@echo "To upload the content to web: make upload, which will send the html files to web location"
	@echo "to clean: make clean, removes all temporary files. make superclean additionally removes all _docs"

render: *.qmd, _quarto.yml ## render html
	quarto render --to html

pdf:	*.qmd, _quarto.yml ## render PDF
	quarto render --to pdf

.PHONY: upload
upload:
	#rsync --mirror ...

.PHONY: clean
clean:  ## removes all temporary files
	rm wget-log* *.aux *.log *.tex *.toc *.pdf
superclean: ## clean + removes all generated dcouments
	rm -fr _docs wget-log* *.aux *.log *.tex *.toc *.pdf


